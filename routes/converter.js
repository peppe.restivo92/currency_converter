const express = require('express');

const converter = require('../controllers/converter');
const validators = require('../utils/validators');
const errorHandler = require('../utils/errorHandler');

const router = express.Router();

const refreshCacheData = async (req, res, next) => {
  await converter.refreshExchangeData(req.re);
  next();
};

/* GET home page. */
router.get('/', validators.checkConvertParams, refreshCacheData, errorHandler(async (req, res) => {
  const result = await converter
    .convert(req.query.reference_date, req.query.src_currency, req.query.dest_currency, req.query.amount);
  await res.json(result);
}));

module.exports = router;
