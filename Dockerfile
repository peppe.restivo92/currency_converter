FROM node:12.14.1

WORKDIR /usr/src/app

ENV NODE_ENV=production

RUN npm install pm2 -g

COPY package*.json /usr/src/app/
RUN npm install --production

COPY . /usr/src/app

ENV PORT 5000

EXPOSE $PORT

#CMD [ "npm", "start" ]
CMD [ "pm2-runtime", "npm", "--", "start" ]
