const request = require('supertest');
const app = require('../app');

// eslint-disable-next-line no-undef
describe('App', () => {
  // eslint-disable-next-line no-undef
  it('has the /convert route', (done) => {
    request(app)
      .get('/convert')
      .query({
        reference_date: '2020-01-03T20:12:0.321', src_currency: 'EUR2', dest_currency: 'ABC', amount: '1a',
      })
      .expect([{
        value: '2020-01-03T20:12:0.321',
        msg: 'DATE_FORMAT_NOT_VALID',
        param: 'reference_date',
        location: 'query',
      },
      {
        value: '1a',
        msg: 'AMOUNT_NOT_VALID',
        param: 'amount',
        location: 'query',
      },
      {
        value: 'EUR2',
        msg: 'CURRENCY_NOT_VALID',
        param: 'src_currency',
        location: 'query',
      }], done);
  });
});
