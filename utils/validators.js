const { check, validationResult } = require('express-validator');

exports.checkConvertParams = [
  check('reference_date')
    .isISO8601({ strict: true })
    .withMessage('DATE_FORMAT_NOT_VALID')
    .bail()
    .isLength({ min: 10, max: 10 }).withMessage('DATE_FORMAT_NOT_VALID'),
  check('amount')
    .exists().withMessage('AMOUNT_NOT_FOUND')
    .bail()
    .isNumeric()
    .withMessage('AMOUNT_NOT_VALID')
    .bail()
    .isCurrency({
      allow_negatives: false,
      decimal_separator: '.',
      thousands_separator: '',
    }),
  check(['src_currency', 'dest_currency'])
    .isLength({ min: 3, max: 3 }).withMessage('CURRENCY_NOT_VALID'), // todo check if it is always true
  checkErrors,
];

function checkErrors(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422)
      .json(errors.array());
  }
  return next();
}
