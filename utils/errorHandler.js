const errHandler = (fnct) => (req, res, next) => {
  Promise.resolve(fnct(req, res, next)).catch((e) => {
    if (e.message) {
      next(e.message);
    } else {
      next(e);
    }
  });
};

module.exports = errHandler;
