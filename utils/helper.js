const xml2js = require('xml2js');

const parser = xml2js.Parser();

exports.getNextWorkDay = () => {
  const nextBusinessDay = new Date();
  nextBusinessDay.setDate(nextBusinessDay.getDate() + 1); // tomorrow

  if (nextBusinessDay.getDay() === 0) nextBusinessDay.setDate(nextBusinessDay.getDate() + 1);
  else if (nextBusinessDay.getDay() === 6) nextBusinessDay.setDate(nextBusinessDay.getDate() + 2);

  nextBusinessDay.setHours(19, 0, 0); // I want to refresh after day
  return nextBusinessDay;
};

exports.parseRemoteExchange = async (data) => {
  const exchange = [];
  await parser.parseString(data, (err, result) => {
    const cubes = result['gesmes:Envelope'].Cube[0].Cube;
    cubes.forEach((cube) => {
      const { time } = cube.$;
      const exchangeRates = [];
      cube.Cube.forEach((detail) => {
        exchangeRates.push({
          rate: detail.$.rate,
          currency: detail.$.currency,
        });
      });
      exchangeRates.push({
        rate: 1,
        currency: 'EUR',
      });
      exchange.push({
        reference_date: time,
        rates: exchangeRates,
      });
    });
  });
  return exchange;
};
