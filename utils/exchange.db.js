const axios = require('axios');

const helper = require('./helper');

let redis;

exports.setRedis = (redisClient) => {
  redis = redisClient;
};

exports.refreshExchangeData = async () => {
  const empty = (await redis.keys('*')).length === 0;

  if (empty) {
    console.info('Redis cache is empty. Download last 90 days exchange data');
    const response = await axios.request({
      url: 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml',
      method: 'GET',
    });
    const remoteExchange = await helper.parseRemoteExchange(response.data);

    const today = new Date();
    const nextRefresh = helper.getNextWorkDay();
    const exp = (nextRefresh - today) / 1000; // seconds

    await writeOnCache(remoteExchange, exp);
    return true;
  }
  return false;
};

exports.getCurrencies = async (referenceDate, srcCurrency, destCurrency) => {
  const currencies = (await redis.hmget(referenceDate, srcCurrency, destCurrency))
    .filter((value) => value)
    .map((value) => Number(value));

  if (currencies.length !== 2) {
    throw new Error('PARAMS_NOT_FOUND');
  }
  return currencies;
};

const writeOnCache = async (exchanges, exp) => {
  await redis.flushall();
  const multi = redis.multi();
  await exchanges.forEach((exchange) => {
    const hash = {};
    const key = exchange.reference_date;
    exchange.rates.forEach((rate) => {
      hash[rate.currency] = rate.rate;
    });
    multi.hmset(key, hash);
    multi.expire(key, exp);
  });
  await multi.exec_batch();
};
