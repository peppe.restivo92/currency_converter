const dbManager = require('../utils/exchange.db');

exports.convert = async (referenceDate, srcCurrency, destCurrency, amount) => {
  const currencies = await dbManager
    .getCurrencies(referenceDate, srcCurrency, destCurrency);

  let amountConverted = (Number(amount) * currencies[1]) / currencies[0];
  amountConverted = (Math.round(amountConverted * 100) / 100).toFixed(2);
  return {
    amount: Number(amountConverted),
    currency: destCurrency,
  };
};

exports.refreshExchangeData = async (redis) => {
  const updated = await dbManager.refreshExchangeData(redis);
  if (updated) {
    console.info('Exchange updated');
  }
};
