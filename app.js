const redis = require('async-redis');
const express = require('express');
const createError = require('http-errors');
const morganBody = require('morgan-body');

const client = redis
  .createClient(process.env.REDIS_PORT || 6379, process.env.REDIS_HOST || 'localhost');
require('./utils/exchange.db')
  .setRedis(client);

const convertRouter = require('./routes/converter');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
morganBody(app, { dateTimeFormat: 'iso', theme: 'dracula' });

app.use('/convert', convertRouter);
app.get('/', (req, res, next) => {
  res.status(200).send();
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // console.error(err);
  res.status(err.status || 500).send(err);
});

module.exports = app;
