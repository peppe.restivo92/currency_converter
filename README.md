# Exchange Converter

## Project first time setup
1\. Install git flow, see https://danielkummer.github.io/git-flow-cheatsheet/
Then:
```
git flow init
git checkout develop
```
2\. npm version suggested 6.12.x , min version required 6.4.1

## Update dependencies
Run when package.json changes
```
npm install
```

### Compiles and hot-reloads for development
In order to run this node application, redis is required.

Install Redis, then
```
npm run dev
```

### Run your tests
```
npm run test
```

## Docker usage
Run the docker-compose.yaml file. This will build the application and then run redis and node services together
 
```
docker-compose up --build
```

Then try with: GET http://localhost:8080/convert?reference_date=2019-12-30&src_currency=USD&dest_currency=EUR&amount=1

## Test in dev environment
This application is deployed on AWS.
The base endpoint is 
```
http://igenius-1835504734.us-east-1.elb.amazonaws.com
```

## TODO List
* [ ] Use fallback on reference_date in case the date is between yesterday and 90days ago. (Holidays)
* [ ] Add MongoDB to store the oldest exchange rate days (When redis expire data, I want to store these rates on MongoDB) 
* [ ] Add more tests

## Hotfix version
Release an hotfix, an hotfix is a partial or bugfix release

An hotfix will use master as base branch 

1 open new release branch
```
git checkout master
git pull
git flow hotfix start -F <version>
```
2 Merge the commits you want to release in this hotfix

3 Increment version before closing HF branch
```
npm run version-hotfix
git commit -m "hotfix version increase"
```

4 Finish the hotfix branch ( you may have conflicts and need to merge them)
```
git flow hotfix finish -Fp <version>
```

5 push your changes to remote
```
git push origin --tags
```

## Release version
Release version is a full release from develop branch

A release will always contain all changes from develop

1 open new release branch
```
git checkout develop
git pull
git flow release start -Fp <version>
```
2 Adjust code if necessary

3 Finish the release branch ( you may have conflicts and need to merge them)
```
git flow release finish -Fp <version>
```

4 push your changes to remote
```
git push origin --tags
```
